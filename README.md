# MERN-Booking-app

⚠️ Please note that the app is currently under development and is not ready for production use. (incompleted) ⚠️

## Getting started
Thanks for checking out this Application. 🎉



## My process
# Built with

- React 
- Vite
- Node
- Axios
- Express
- Mongo DB
- Mongoose

## Author 👩‍💻
- Linkedin - [@aous-shaheen-381636221](https://www.linkedin.com/in/shaheen2001/)
- Facebook - [@aoushaheen7](https://www.facebook.com/shaheen72001/)


I appreciate your interest in Skill Up Stay tuned for future updates and the official release of the app. If you have any questions or feedback, feel free to reach out. 😊🚀