// import express from "express";
const express = require("express");
const cors = require("cors");
const { default: mongoose } = require("mongoose");
const User = require("./models/User.js");
require("dotenv").config();

//!! App created
const app = express();

app.use(express.json());

//!! App CORS
app.use(
  cors({
    credentials: true,
    origin: "http://localhost:5173",
  })
);

mongoose.connect("mongodb+srv://shaheen:shaheenbooking@cluster0.jxcjyy7.mongodb.net/?retryWrites=true&w=majority");

//!! App Routers
app.get("/test", (req, res) => {
  res.json("test oK");
});

app.post("/register", (req, res) => {
  const { name, email, password } = req.body;
  User.create({
    name,
    email,
    password,
  });
});

app.listen(4000);

//
